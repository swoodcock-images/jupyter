#!/bin/bash
set -euo pipefail

export DOCKER_BUILDKIT=1

source .env

docker build . --pull \
      --file Dockerfile \
      --tag "${REGISTRY}notebook:$PYTHON_VERSION" \
      --build-arg PYTHON_VERSION=$PYTHON_VERSION \
      --build-arg NOTEBOOK_IMG_TAG=$NOTEBOOK_IMG_TAG \
      --build-arg http_proxy=$proxy_url \
      --build-arg https_proxy=$proxy_url \
      --build-arg no_proxy=$no_proxy \
      --build-arg MAINTAINER=$MAINTAINER \
      --build-arg NB_USER=$NB_USER \
      --build-arg NB_UID=$NB_UID \
      --build-arg NB_GID=$NB_GID
docker push "${REGISTRY}notebook:$PYTHON_VERSION"